FROM centos:7

ENV VER "0.13.0-beta3"

RUN yum update -y && yum install wget -y && yum install unzip -y
RUN yum install epel-release -y && yum install ansible -y
RUN yum -y install openssh-server openssh-clients
RUN yum install which -y
RUN systemctl enable sshd

RUN wget https://releases.hashicorp.com/terraform/${VER}/terraform_${VER}_linux_amd64.zip
RUN unzip terraform_${VER}_linux_amd64.zip
RUN mv terraform /usr/local/bin/  
RUN rm -rf terraform_${VER}_linux_amd64.zip
RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;
VOLUME [ "/sys/fs/cgroup" ]
CMD ["/usr/sbin/init"]